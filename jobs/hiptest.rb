require 'net/http'
require 'json'

# curl --header 'Accept: application/vnd.api+json; version=1' --header 'access-token: -WRID1-gElBCJJ5gIz46rw' --header 'client: nKkzGezV7_A92WQcEu_MBA' --header 'uid: jasontur@rccl.com' https://app.hiptest.com/api/projects/90151/test_runs

# curl "https://app.hiptest.com/api/projects/90151/test_runs/81965?include=dataset,scenario&show_passrate" \
    # -H 'accept: application/vnd.api+json; version=1' \
    # -H 'access-token: -WRID1-gElBCJJ5gIz46rw' \
    # -H 'uid: jasontur@rccl.com' \
    # -H 'client: nKkzGezV7_A92WQcEu_MBA'

    # curl "https://app.hiptest.com/api/projects/90151/test_runs/81965/test_snapshots" \
    # -H 'accept: application/vnd.api+json; version=1' \
    # -H 'access-token: -WRID1-gElBCJJ5gIz46rw' \
    # -H 'uid: jasontur@rccl.com' \
    # -H 'client: nKkzGezV7_A92WQcEu_MBA'

    # GET https://app.hiptest.com/api/projects/90151/test_runs/81965/test_snapshots HTTP/1.1
    # Accept: application/vnd.api+json; version=1
    # access-token:-WRID1-gElBCJJ5gIz46rw
    # client: nKkzGezV7_A92WQcEu_MBA
    # uid: jasontur@rccl.com'


    # Set the following constants with your data
PROJECT_ID = 0
ACCESS_TOKEN = '-WRID1-gElBCJJ5gIz46rw'
CLIENT_ID = 'nKkzGezV7_A92WQcEu_MBA'
UID = 'jasontur@rccl.com'


PROJECT_URL = "https://app.hiptest.com/api/projects/90151/test_runs"

# That method will actually fetch the data from Hiptest
# and return an array of hashes containing test runs names and statuses
def request_hiptest_status

  uri = URI(PROJECT_URL)
  result = Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') do |http|
    request = Net::HTTP::Get.new uri
    request['Accept'] = "application/vnd.api+json; version=1"
    request['access-token'] = ACCESS_TOKEN
    request['client'] = CLIENT_ID
    request['uid'] = UID
    http.request request
  end

  if result and result.is_a?(Net::HTTPOK)
    response = JSON.parse(result.body)

    # To return an array containing only names and statusese of test runs
    return response['data'].collect do |test_run|
      {
        'name' => test_run['attributes']['name'], 
        'statuses' => test_run['attributes']['statuses']
      }
    end
  end

  # If something wrong happened then tiles won't be refreshed.
  puts 'An error occurs.'
  puts result

  return nil
  
end

# This method is in charge of returning the most
# valuable status for the given statuses.
#
# It's up to you to define here which status you want your
# dashboard to show depending the statuses of a test run
def get_status_text(statuses)

  return "Failed" if statuses['failed'] > 0
  return "Blocked" if statuses['blocked'] > 0
  return "Skipped" if statuses['skipped'] > 0
  return "Work in progress" if statuses['wip'] > 0 || statuses['undefined'] > 0
  return "Retest" if statuses['retest'] > 0
  return "Passed" if statuses['passed'] > 0

  return "Unknown"

end

# This will simply concatenate the statuses
# into a single string
def get_status_details(statuses)

  return statuses.map { |key, value|
    "#{key}: #{value}" if value > 0
  }.join(' ')

end

# Every 10 seconds the dashboard will fetch statuses from Hiptest
# then refresh the tiles accordingly
SCHEDULER.every '10s' do

  test_runs = request_hiptest_status

  unless test_runs.nil?

    send_event(
      'tr-1', 
      { 
        title: test_runs[1]['name'], 
        text: get_status_text(test_runs[1]['statuses']),
        moreinfo: get_status_details(test_runs[1]['statuses'])
      })

    send_event(
      'tr-2', 
      { 
        title: test_runs[0]['name'], 
        text: get_status_text(test_runs[0]['statuses']),
        moreinfo: get_status_details(test_runs[0]['statuses'])
      })
      send_event(
        'tr-3', 
        { 
          title: test_runs[2]['name'], 
          text: get_status_text(test_runs[2]['statuses']),
          moreinfo: get_status_details(test_runs[2]['statuses'])
        })
        send_event(
          'tr-4', 
          { 
            title: test_runs[10]['name'], 
            text: get_status_text(test_runs[10]['statuses']),
            moreinfo: get_status_details(test_runs[10]['statuses'])
          })
  end

end